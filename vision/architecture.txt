   		       	      
   CORE INFRASTRUCTURE 	       	       	       	       	       	 
  =====================	     					 
   		       	     					 
   	 +--------------------+					 
   	 |	       	      |					 
   	 | project management |					 
    	 |	       	      |					 
         +--------------------+					 
   		 |     	     					 
   		 |	      					 
    		 v     	     					 
   	   +---------------+					 
   	   |	       	   | 					 	    +------------+	   
   	   | serialization |  					 	    |	       	 |	   
   	   |	       	   |  	     				 	    | Blackboard |	   
    	   +---------------+  					 	    | 	   	 |	   
   		 |     	      	   				 	    +------------+	   
   		 |     	      	   				 		   |	       	    
   		 |     	      	   				 		   |	 	    
   		 |     	  +--------------------------------------------------------+---------------+
       	       	 |     	  |   	 				 		   		   |
   		 |     	  |   	 				 				   |
    		 |     	  +-- CLI (execute known functions)	 				   |
   	       	 |     	  |    		       			 				   |
   		 |     	  |   	  	      	       		 				   |
   		 |     	  |   	  +---------- interface for different languages		  	   |
   		 |     	  |   	  |				 			  	   |
   	     	 |     	  |   	  |				 			  	   |
   	     	 v     	  v    	  v				 			  	   v   
   	   +- internal representation -+       			  			   +----------+
     	   |   	       	       	       |       	       	       	 =========     	       	   |   	      |
       	   |   function <---> data     | <---------------provide |Modules| provide-------> | graphics |
	   |			       |		     	 =========		   | 	      |
	   +---------------------------+		     	  	  		   +----------+
							   
							    



