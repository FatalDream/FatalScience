
#![feature(quote, plugin_registrar, rustc_private)]

// libs
extern crate rustc;
extern crate rustc_plugin;
extern crate syntax;
extern crate num;
#[macro_use] extern crate lazy_static;

use std::sync::Mutex;
use rustc_plugin::Registry;
use syntax::ast::{self, ItemKind, TokenTree, MetaItem};
use syntax::parse::token::{self, str_to_ident};
use syntax::ext::base::*;
use syntax::codemap::{self, Span};
use syntax::util::small_vector::SmallVector;

// local
extern crate reflection;

mod ast_transform;
mod error;

use reflection::*;
use reflection::function::*;
use ast_transform::*;
use error::*;


///////////////////////////////////////////////////////////////
// static, persistent var
lazy_static! {
    static ref REG_FUNCTIONS: Mutex<Vec<RFunctionDecl>> = Mutex::new(Vec::new());
}


///////////////////////////////////////////////////////////////
// public functions
#[plugin_registrar]
pub fn registrar(reg: &mut Registry) {
    reg.register_syntax_extension(token::intern("rfunction"), MultiDecorator(Box::new(expand)));
    reg.register_macro("rinit", expand_rinit);
}

fn expand_rinit(cx: &mut ExtCtxt, _: codemap::Span, _: &[ast::TokenTree]) -> Box<MacResult>
{
    
    // get the function declarations which were registered up until now
    let ref decls : Vec<RFunctionDecl> = *REG_FUNCTIONS.lock().unwrap();

    // convert them to tokens and intersperse with commas
    let decl_tokens : Vec<TokenTree> = decls
        .iter()
        .map(|decl| functiondecl_to_tokens(decl, cx))
        .collect::<Vec<_>>()
        .join(&TokenTree::Token(codemap::DUMMY_SP, token::Comma));

    
    // create function to return these function declarations
    let fname = str_to_ident(&naming::get_functionlist_name());
    let raw_function = quote_item!(
        cx,
        //--------------- generated code -------------
        pub fn $fname() -> Vec<reflection::function::RFunctionDecl>
        {
            vec!($decl_tokens)
        }
        //--------------------------------------------
    ).unwrap();
    let annotated_function = raw_function.map(
        |f| item_with_attr(f,
                           quote_attr!(
                               cx,
                               //--- generated ---
                               #[no_mangle]
                               //-----------------
                           ))).map(
        |f| item_with_attr(f,
                           quote_attr!(
                               cx,
                               //--- generated ---
                               #[allow(dead_code)]
                               //-----------------
                           )));

    MacEager::items(SmallVector::many(vec!(annotated_function)))
}

fn expand(
    cx:   &mut ExtCtxt,
    span:  Span,
    _:   &MetaItem,
    it:   &Annotatable,
    push: &mut FnMut(Annotatable))
{

    // // get the passed MetaItem args
    // match mi.node {
    //     ast::MetaItemKind::List(_, ref xs) => {
    //         if let MetaItemKind::Word(ref w) = xs[0].node {
    //             str_to_ident(&w)
    //         } else {
    //             //cx.span_err(mi.span, "Expected word");
    //             //return;
    //         }
    //     }
    //     _ => {
    //         //cx.span_err(mi.span, "Expected list");
    //         //return;
    //     }
    // };

    // cover the different types of annotations
    let result = match it.clone() {
        
        Annotatable::Item(item) => {
            // identificators of the annotated function and of the generated one
            let original_ident = item.ident;
            let caller_ident = str_to_ident(&naming::get_caller_name(&ident_to_string(&original_ident)));

            // proceed only if the item is a function
            match item.node {
                ItemKind::Fn(ref decl, _, _, _, _, _) => {

                    // create custom function declaration type
                    let rfndecl = create_functiondecl(decl, &original_ident, &caller_ident);

                    // variables needed inside the generated code
                    let args_name = str_to_ident("args");
                    let args_len = rfndecl.inputs.len();


                    // generate the argument extractions
                    let generate_typecast = |tok_arg_num: u32| {
                        quote_tokens!(
                            cx,
                            //---------- generated code ------------
                            if let Some(val) = $args_name[$tok_arg_num as usize].downcast_ref() {
                                val
                            } else {
                                return Err("Wrong param type");
                                    //reflection::ReflError::WrongParamType($tok_arg_num));
                            }
                            //--------------------------------------
                        )
                    };
                    let args = num::iter::range(0, rfndecl.inputs.len() as u32)
                        .map(generate_typecast)
                        .collect::<Vec<_>>()
                        .join(&TokenTree::Token(codemap::DUMMY_SP, token::Comma));
                    
                    // generate the caller function containing them
                    let raw_caller_fn = quote_item!(
                        cx,
                        //--------- generated code -----------
                        pub fn $caller_ident($args_name: Vec<Box<std::any::Any>>) -> reflection::ReflResult<Box<std::any::Any>>
                        {
                            match $args_name.len() {
                                $args_len => Ok(Box::new($original_ident($args))),
                                _             => Err("wrong number of params")
                                //reflection::ReflError::WrongNumberOfParameters)
                            }
                        }
                        //------------------------------------
                    ).unwrap();
                    let generated_caller_fn = raw_caller_fn.map(
                        |f| item_with_attr(f,
                                           quote_attr!(
                                               cx,
                                               //--- generated ---
                                               #[allow(dead_code)]
                                               //-----------------
                                           ))).map(
                        |f| item_with_attr(f,
                                           quote_attr!(
                                               cx,
                                               //--- generated ---
                                               #[no_mangle]
                                               //-----------------
                                           )));

                    // write generated caller into syntax tree
                    push(Annotatable::Item(generated_caller_fn));

                    // register function persistently, for rinit to find
                    REG_FUNCTIONS.lock().unwrap().push(rfndecl);

                    RFunctionAnnotationResult::Success
                }
                
                _ => RFunctionAnnotationResult::NotAFunction(span)
            }
        }
        
        _ => RFunctionAnnotationResult::NotAFunction(span)
    };

    result.notify_user(cx);
}
