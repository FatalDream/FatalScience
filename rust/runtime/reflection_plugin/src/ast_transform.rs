
// compiler import
extern crate syntax;

use syntax::ast::{Attribute, Ident, Item, TokenTree, FnDecl, FunctionRetTy, Pat, PatKind};
use syntax::parse::token::{self};
use syntax::ext::base::*;
use syntax::print::pprust;
use syntax::codemap;

// fatalscience import
extern crate reflection;

use reflection::*;
use reflection::function::*;

///////////////////////////////////////////////////////////////////
// public functions

pub fn ident_to_string(i: &Ident) -> String
{
    let mut name = String::new();
    name.push_str(&i.name.as_str());
    name
}

pub fn create_functiondecl(decl: &FnDecl, name: &Ident, caller_name: &Ident) -> RFunctionDecl
{
    fn extract_idents(pat: &Pat, idents: &mut Vec<Ident>) {
        match pat.node {
            PatKind::Wild | PatKind::Mac(_) 
                | PatKind::Lit(_)
                | PatKind::Range(..) | PatKind::QPath(..) | PatKind::Path(_) | PatKind::Ref(..) => (),
            PatKind::Ident(_, sp, _) => if sp.node.name.as_str() != "self" { idents.push(sp.node) },
            PatKind::Tup(ref v) => {
                for p in v {
                    extract_idents(&p, idents);
                }
            }
            PatKind::Struct(_, ref v, _) => {
                for p in v {
                    extract_idents(&p.node.pat, idents);
                }
            },
            PatKind::TupleStruct(_, ref opt) => {
                if let &Some(ref v) = opt {
                    for p in v {
                        extract_idents(&p, idents);
                    }
                }
            }
            PatKind::Vec(ref v1, ref opt, ref v2) => {
                for p in v1 {
                    extract_idents(&p, idents);
                }
                if let &Some(ref p) = opt {
                    extract_idents(&p, idents);
                }
                for p in v2 {
                    extract_idents(&p, idents);
                }
            }
            PatKind::Box(ref p)
                => extract_idents(&p, idents),
                
        }
    }

    let mut args = vec!();
    for arg in decl.inputs.iter() {
        let mut idents = vec!();
        extract_idents(&arg.pat, &mut idents);
        let param = RFunctionParam::new(
            ident_to_string(&idents[0]),
            pprust::ty_to_string(&(*arg.ty))
        );
        args.push(param);
    }

    let ret_ty : RType = match &decl.output {
        &FunctionRetTy::Ty(ref ty) => pprust::ty_to_string(&*ty),
        _                          => "()".to_string()
    };
    
    RFunctionDecl::new(ident_to_string(name), ident_to_string(caller_name), args, ret_ty)
}

pub fn functiondecl_to_tokens(f: &RFunctionDecl, cx: &ExtCtxt) -> Vec<TokenTree>
{
    let ref name = f.name;
    let ref caller_name = f.caller_name;
    let ref inputs = f.inputs;
    let ref output = f.output;

    let tok_inputs : Vec<TokenTree> = inputs
        .iter()
        .map(|i| rparam_to_tokens(i, cx))
        .collect::<Vec<_>>()
        .join(&TokenTree::Token(codemap::DUMMY_SP, token::Comma)); 

    quote_tokens!(
        cx,

        //-------------- generated code -------------
        reflection::function::RFunctionDecl
        {
            name: $name.to_string(),
            caller_name: $caller_name.to_string(),
            inputs: vec!($tok_inputs),
            output: $output.to_string()
        }
        //-------------------------------------------
    )   
}

pub fn rparam_to_tokens(p: &RFunctionParam, cx: &ExtCtxt) -> Vec<TokenTree>
{
    let ref name = p.name;
    let ref ty = p.ty;

    quote_tokens!(
        cx,

        //------------ generated code ------------
        reflection::function::RFunctionParam
        {
            name: $name.to_string(),
            ty: $ty.to_string()
        }
        //----------------------------------------
    )   
}

pub fn item_with_attr(i: Item, a: Attribute) -> Item
{
    let Item {
        ident: o_ident,
        attrs: o_attrs,
        id: o_id,
        node: o_node,
        vis: o_vis,
        span: o_span
    } = i;

    let mut new_attrs = o_attrs;
    new_attrs.push(a);

    Item {
        ident: o_ident,
        attrs: new_attrs,
        id: o_id,
        node: o_node,
        vis: o_vis,
        span: o_span
    }
}
