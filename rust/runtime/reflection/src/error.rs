

use std::convert;
use std::io;

#[derive(Debug)]
pub enum Error {
    ModuleNotFound(String),
    FunctionNotFound(String),
    FunctionNameAmbigious(String),
    LibraryWithoutReflection(String),
    IO(io::Error),
}

impl Error {
    pub fn to_string(&self) -> String {
        match self {
            &Error::ModuleNotFound(ref s) => "Module '".to_string() + &s + "' could not be found",
            &Error::FunctionNotFound(ref s) => "The function '".to_string() + &s + "' could not be found.",
            &Error::FunctionNameAmbigious(ref s) => s.to_string(),
            &Error::LibraryWithoutReflection(ref s) => format!("Library '{:?}' appears to be without reflection", s),
            &Error::IO(ref e) => format!("IO error occurred: '{:?}'.", e)
                
        }
    }
}

