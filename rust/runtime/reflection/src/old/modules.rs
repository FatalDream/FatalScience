
#[macro_use] extern crate lazy_static;

use self::dynamic_reload::{DynamicReload, Lib, Symbol, Search, PlatformName, UpdateState};
use std::rc::Rc;

// #[derive(PartialEq)]
// struct Module
// {
//     name: String,
//     lib: Lib
// }

type Module = Lib;

struct Modules
{
    modules: Vec<Rc<Module>>
}

impl Modules
{
    ///////////////////////////////////////////
    // ctor
    fn new() -> Modules
    {
        Modules { modules: vec!() }
    }
    
        
    ///////////////////////////////////////////
    // access
        
    fn add(&mut self, module: &Rc<Module>)
    {
        self.modules.push(module.clone());
    }
    fn unload(&mut self, module: &Rc<Module>)
    {
        for i in (0..self.modules.len()).rev() {
            if &self.modules[i] == module {
                self.modules.swap_remove(i);
            }
        }
    }
    fn reload(&mut self, module: &Rc<Module>)
    {
        Self::add(self, module);
    }

    fn reload_callback(&mut self, state: UpdateState, lib: Option<&Rc<Lib>>)
    {
        match state {
            UpdateState::Before          => self.unload(lib.unwrap()),
            UpdateState::After           => self.reload(lib.unwrap()),
            UpdateState::ReloadFalied(e) => println!("reload failed: {:?}", e)
        }
    }
}


