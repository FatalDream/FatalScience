//#![feature(rustc_private, quote)]

// extern crate syntax;

// use syntax::ext::*;
// use syntax::ext::base::*;
// use syntax::ast::{self, Item, ItemKind, MetaItem, Ident, TokenTree, FnDecl, Pat, PatKind};
// use syntax::codemap;
// use syntax::parse::token;


#[macro_use] extern crate lazy_static;

mod error;
pub use self::error::Error;

pub mod function;
pub mod naming;
pub mod module_loader;

pub enum ReflError {
    WrongNumberOfParameters,
    WrongParamType(u32)
}

pub type ReflResult<T> = Result<T, Error>;



