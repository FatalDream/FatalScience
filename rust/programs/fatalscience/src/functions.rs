
extern crate std;
extern crate reflection;

#[rfunction(nop)]
pub fn to_upper(a: &String) -> String {
    a.clone()
}
