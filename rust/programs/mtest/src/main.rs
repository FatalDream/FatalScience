
extern crate reflection;
use reflection::*;

fn main() {

    let mut loader = module_loader::ModuleLoader::new();
    loader.load_modules();
    let res = loader.get_module(&"misc".to_string())
        .and_then(|m| m.get_function("get_num".to_string()))
        .and_then(|f| f.call(vec!()));
    println!("called moduleloader, got: {:?}", res);
    println!("res is: {}", res.unwrap().downcast_ref::<i32>().unwrap());
}
