
#![feature(plugin)]

#![plugin(reflection_plugin)]
extern crate reflection;

use reflection::function;

#[rfunction]
pub fn get_num() -> i32 {
    42
}

#[rfunction]
pub fn fact(a: &i32) -> i32 {
    match a.clone() {
        0 => 1,
        n => n * fact(&(n - 1)),
    }
}

rinit!();


